import * as todoActionTypes from "../actionTypes/todoActionTypes";
import {ItodoReducerState} from "./types";

const initialState: ItodoReducerState = {
  todos: [
    {
      name: "hello",
      completed: false
    }, {
      name: "hello 654",
      completed: true
    }, {
      name: "Good Night",
      completed: false
    }, {
      name: "React-redux",
      completed: true
    }
  ],
  loading: false
};

export const todoReducers = (state : ItodoReducerState = initialState, action : any) => {
  switch (action.type) {
    case todoActionTypes.REQUEST_ALL_TODO:
      return {
        ...state
      };
    case todoActionTypes.REQUEST_ALL_TODO_SUCCESS:
      return {
        ...state
      };
    case todoActionTypes.REQUEST_ALL_TODO_ERROR:
      return {
        ...state
      };
    default:
      return state;
  }
};
