export interface todos {
  name: string;
  completed?: boolean;
}
export interface ItodoReducerState {
  todos: todos[];
  loading: boolean;
}
