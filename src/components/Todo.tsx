import React from "react";
import {TodoItemsList} from "./TodoItemsList";
import {connect} from "react-redux";

interface ITodoProps {
  todos: [];
}

interface ITodoState {}

class Todo extends React.Component<ITodoProps, ITodoState> {
  state: ITodoState = {};

  render() {
    const {todos} = this.props;
    return (<React.Fragment>
      <div className="col-md-6 offset-md-3">
        <div className="card mt-5">
          <div className="card-header bg-info text-white">
            <h3>Todo List</h3>
          </div>
          <div className="card-body">
            <ul className="list-group">
              {todos.map((todo, index) => (<TodoItemsList todo={todo} key={index} index={index}/>))}
            </ul>
          </div>
        </div>
      </div>
    </React.Fragment>);
  }
}

const mapStateToProps = (state : any) => ({todos: state.todo.todos});

const mapDispatchToProps = (dispatch : any) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Todo);
