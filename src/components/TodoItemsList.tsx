import React from "react";
import {todos} from "../reducers/types";

interface ITodoItemsList {
  todo: todos;
  index: number;
}

export const TodoItemsList = ({todo, index} : ITodoItemsList): JSX.Element => {
  return (<React.Fragment>
    <li className="list-group-item mb-2">
      <div className="custom-control custom-checkbox mr-sm-2">
        <input type="checkbox" className="custom-control-input" id={"customControlAutosizing" + index} defaultChecked={todo.completed}/>
        <label className="custom-control-label" htmlFor={"customControlAutosizing" + index}>
          <React.Fragment>
            <p style={{
                textDecoration: todo.completed
                  ? "line-through"
                  : "none"
              }}>
              {todo.name}
            </p>
          </React.Fragment>
        </label>
      </div>
    </li>
  </React.Fragment>);
};
