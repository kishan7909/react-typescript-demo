import React from "react";
import Todo from "./Todo";

interface IHomeProps {}

interface IHomeState {}

class Home extends React.Component<IHomeProps, IHomeState> {
  state: IHomeState = {};
  render() {
    return (<div>
      <Todo/>
    </div>);
  }
}

export default Home;
