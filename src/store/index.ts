import {createStore, applyMiddleware} from "redux";
import createSagaMiddleware from "redux-saga";
import {composeWithDevTools} from "redux-devtools-extension/developmentOnly";
import {rootReducer} from "../reducers";
import {rootSaga} from "../saga";

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(rootReducer, composeWithDevTools({trace: true})(applyMiddleware(sagaMiddleware)));

sagaMiddleware.run(rootSaga);
